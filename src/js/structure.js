import * as CRUD from './crud-provider';

const body = document.body;

let cmbSelect, count, options, Id, Name, Job, Create, Update, Delete;

const createHtmlGetUser = () => {
    const html = `
    <h1 class="mt-4">App CRUD Users</h1>
    <hr>
    <form>
        <fieldset>
            <legend>Get users to API</legend>

            <div class="mb-3">
                <div class="mt-2 form d-flex">
                    <select id="cmbSelect" placeholder="Pages" class="form-select w-50 mr-2">
                        <option > </option>
                        
                    </select>
                </div>
            </div>

        </fieldset>
    </form>

    <table class="table text-center">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Avatar</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Email</th>
                <th scope="col">Id</th>
            </tr>
        </thead>
        <tbody>
           

        </tbody>
    </table>
    <br>
    `;

    const div = document.createElement('div');
    div.innerHTML = html;
    body.append(div);

    cmbSelect = document.querySelector('#cmbSelect');

    count = 0;
    for (let i = 0; i < 12; i++) {
        options = document.createElement('option');
        options.value = count + 1;
        options.innerText = `User # ${count +1}`;

        count++;
        cmbSelect.append(options);
    }
    // cmbSelect.innerHTML = options;
}

// events
const events = () => {

    cmbSelect = document.querySelector('#cmbSelect');

    cmbSelect.addEventListener('change', async e => {
        console.log(e.target.value);
        const resp = await CRUD.getUser(e.target.value);
        const { avatar, email, first_name, last_name, id } = resp;

        info(resp);
        // console.log(resp);
    });
}

// Datos de la tabla
const info = (data) => {
    let html = `
    <tr>
        <th class="align-middle" scope="row">*</th>
        <td class="align-middle"><img class="img-thumbnail" src="${data.avatar}" alt="img-api"></td>
        <td class="align-middle">${data.first_name}</td>
        <td class="align-middle">${data.last_name}</td>
        <td class="align-middle">${data.email}</td>
        <td class="align-middle">${data.id}</td>
    </tr>
    `;

    document.querySelector('tbody').innerHTML = html;
}


// CRUD USERS

const createHtmlCRUD = () => {
    const html = `
    <form>
        <fieldset>
            <legend>Create-Update-Delete user in API</legend>
            <br>
            <div class="mb-3">
                <div class="mt-2 form d-flex">
                    <div class="w-100 form-floating mb-3">
                        <input id="ID" type="number" class="form-control" id="floatingInput" placeholder="">
                        <label for="floatingInput">Id:</label>
                    </div>
                    <div class="w-100 form-floating mb-3">
                        <input id="NAME" type="text" class="form-control" id="floatingInput" placeholder="">
                        <label for="floatingInput">Name:</label>
                    </div>
                    <div class="w-100 form-floating">
                        <input id="JOB" type="text" class="form-control" id="floatingPassword" placeholder="">
                        <label for="floatingPassword">Job:</label>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
    <div class="d-flex">
        <button id="Create" class="w-100 btn btn-outline-success">Create</button>
        <button id="Update" class="w-100 btn btn-outline-info">Update</button>
        <button id="Delete" class="w-100 btn btn-outline-danger">Delete</button>
    </div>
    <br>

    <table class="table table-striped text-center">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Job</th>
            <th scope="col">CreatedAt</th>
            <th scope="col">Id</th>
        </tr>
    </thead>
    <tbody id="CRUD">
    </tbody>
</table>
    `;

    const div = document.createElement('div');
    div.innerHTML = html;
    body.append(div);
}

// events CRUD
const eveCRUD = () => {
    let id, name, job;
    Id = document.querySelector('#ID');
    Id.addEventListener('change', e => {
        // console.log(e.target.value);
        id = e.target.value;
    });

    Name = document.querySelector('#NAME');
    Name.addEventListener('change', a => {
        name = a.target.value;
    });
    Job = document.querySelector('#JOB');
    Job.addEventListener('change', a => {
        job = a.target.value;
    });

    Create = document.querySelector('#Create');
    Update = document.querySelector('#Update');
    Delete = document.querySelector('#Delete');

    Create.addEventListener('click', async() => {
        console.log('CLick');
        console.log(name, job);

        if (name == null && job == null) {
            alert('Name and Job incomplete');
        } else {

            const userC = await CRUD.createUser({
                name,
                job
            });
            console.log(userC);
            Name.value = '';
            Job.value = '';
            table2(userC, 1)
        }

    });

    Update.addEventListener('click', async() => {
        console.log(id, name, job);

        if (id == null && name == null && job == null) {
            alert('Id, Name and Job incomplete');
        } else {

            const userU = await CRUD.updateUser(id, {
                name,
                job
            });
            Id.value = '';
            Name.value = '';
            Job.value = '';
            table2(userU, 1)
        }
    });

    Delete.addEventListener('click', async() => {
        console.log(id);

        if (id == null) {
            alert('Id incomplete');
        } else {

            const userD = await CRUD.deleteUser(id);
            Id.value = '';

            alert(`Delete User: ${userD}`);
            document.querySelector('#CRUD').innerHTML = '';
        }
    })

}

const table2 = (data, c) => {
    const html = `
        <tr>
            <th class="align-middle" scope="row">${c}</th>
            <td class="align-middle">${data.name}</td>
            <td class="align-middle">${data.job}</td>
            <td class="align-middle">${data.createdAt || data.updatedAt}</td>
            <td class="align-middle">${data.id}</td>
        </tr>
    `;
    // document.querySelector('#CRUD').append(html);
    document.querySelector('#CRUD').innerHTML = html;
}

export const init = () => {
    createHtmlGetUser(),
        events(),
        createHtmlCRUD(),
        eveCRUD()
}

// export {
//     createHtmlGetUser
// }