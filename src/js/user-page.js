import { getUsers } from "./http-provider";

const body = document.body;

let tBody;

const createTablesHtml = () => {
    const html = `
        <h1 class="mt-4">Users</h1>
        <hr>

        <table class="table text-center ">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Avatar</th>
                    <th scope="col">First_name</th>
                    <th scope="col">Last_name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Id</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    `;

    const createDiv = document.createElement('div');

    createDiv.innerHTML = html;
    body.append(createDiv);

    showUser();
}


// Eventos

const showUser = async() => {
    RowUser(await getUsers());

}


// Inserciones


const RowUser = (data) => {

    tBody = document.querySelector('tbody');

    let c = 1;
    data.forEach(element => {

        const etiqueta_tr = document.createElement('tr');
        const etiqueta_th = document.createElement('th');
        const etiqueta_td_img = document.createElement('td');
        etiqueta_td_img.classList.add('align-middle');
        const etiqueta_img = document.createElement('img');

        // Etiqueta th
        etiqueta_th.scope = 'row';
        etiqueta_th.classList.add('align-middle');
        etiqueta_th.innerText = c++;
        etiqueta_tr.append(etiqueta_th);

        // Etiqueta img
        etiqueta_img.classList.add('img-thumbnail');
        etiqueta_img.src = element.avatar;
        etiqueta_td_img.append(etiqueta_img);
        etiqueta_tr.append(etiqueta_td_img);

        // Filas de datos
        let arrContent = [];
        // console.log(element);
        const { first_name, last_name, email, id } = element;
        arrContent.push(first_name, last_name, email, id);
        for (let d of arrContent) {
            const createTd = document.createElement('td');
            createTd.classList.add('align-middle');
            createTd.innerText = d;
            etiqueta_tr.append(createTd);
        }

        // Insercion del body
        tBody.append(etiqueta_tr);
    });

}


export const init = () => {
    createTablesHtml();
}